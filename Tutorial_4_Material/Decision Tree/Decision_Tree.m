function [] = Desision_Tree_Classifier(u)
%
% This function will:
% 
% 1. train a decision tree based on 'training_data.txt' and 'training_class.txt';
% 2. output the confidence interval of the accurancy for a given confidence value;
% 3. predict lables for 'test_data.txt';
% 4. output the average accuracy of this classifier based on 'test_class.txt'.
%
% INPUT: confidence level u e.g. 0.95
% 
%
% author: LI Xiaodong 04/13/2017

% input data A and labels b
[a1 a2 a3 a4]=textread('training_data.txt','%s%s%s%s');
%a2 is not helpful as they are all same with each other
%It asks variance not equal 0
A=[str2double(a1) str2double(a2) str2double(a3) str2double(a4)];
b=textread('training_class.txt','%s');


%train the Decision Tree Classifier by A, b
t = fitctree(A, b,'PredictorNames',{'X[0]' 'X[1]' 'X[2]' 'X[3]'});

%visualize the decision tree 
view(t,'Mode','graph');


%input the test data C and test lable d
[a1 a2 a3 a4]=textread('test_data.txt','%s%s%s%s');
C=[str2double(a1) str2double(a2) str2double(a3) str2double(a4)];
d=textread('test_class.txt','%s');
d=str2double(d);

%visualize the pruned tree with level 2.
pt1 = prune(t,'Level',1);pt2 = prune(t,'Level',2);pt3 = prune(t,'Level',3);pt4 = prune(t,'Level',4);
view(pt2,'Mode','graph')

%predict the test lables 
e0=str2double(predict(t, C));e1=str2double(predict(pt1, C));e2=str2double(predict(pt2, C));e3=str2double(predict(pt3, C));e4=str2double(predict(pt4, C));
%Calculate the average accuracy
Ac=size(1:5);
Ac(5)=Accuracy(d,e0);Ac(4)=Accuracy(d,e1);Ac(3)=Accuracy(d,e2);Ac(2)=Accuracy(d,e3);Ac(1)=Accuracy(d,e4);
Test_Ac=Ac;

%predict the train lables 
e0=str2double(predict(t, A));e1=str2double(predict(pt1, A));e2=str2double(predict(pt2, A));e3=str2double(predict(pt3, A));e4=str2double(predict(pt4, A));
%Calculate the average accuracy
b=str2double(b);
Ac(5)=Accuracy(b,e0);Ac(4)=Accuracy(b,e1);Ac(3)=Accuracy(b,e2);Ac(2)=Accuracy(b,e3);Ac(1)=Accuracy(b,e4);
Train_AC=Ac;

%when will be overfitting? Best choice's right hand
x=[1 2 3 4 5];
plot(x,Train_AC,'b-',x,Test_Ac,'r--')
figure(gcf);
hold on
xlabel('Number of terminal nodes');
ylabel('Accuracy')
legend('Training Data','Test Data')


%confidence interval
k=10;%10 20 30 ...
subsize=size(C,1)/k;
Ac=[1:k];
for i=1:k
e=str2double(predict(t, C(1+subsize*(i-1):subsize*i,:)));
Ac(i)=Accuracy(d(1+subsize*(i-1):subsize*i,:),e);
end
[mu,sigma,muci,sigmaci]=normfit(Ac,(1-u));
fprintf('Confidence Interval of Accuracy: [%f, %f] with confidence level %f\n', muci(1), muci(2), u);