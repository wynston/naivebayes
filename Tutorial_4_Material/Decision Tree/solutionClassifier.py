
# coding: utf-8

# In[2]:

from sklearn import tree
from sklearn.datasets import load_iris
from sklearn.externals.six import StringIO
import pydot
import numpy as np
import random
from numpy.random import RandomState
from scipy import stats
import math


training_data = np.loadtxt('training_data.txt')#,delimiter=','
test_data = np.loadtxt('test_data.txt')#,usecols=[3,4,5,7])[100:]
training_class = np.loadtxt('training_class.txt')#train2.txt',usecols=[8])[:100]
test_class = np.loadtxt('test_class.txt')#train2.txt',usecols=[8])[100:]
#training_data = np.load('training_data.npy')
#test_data = np.load('test_data.npy')
#training_class = np.load('training_class.npy')
#test_class = np.load('test_class.npy')


#building the classifier (the option random_state=RandomState(130) makes the algorithm deterministic)
#min_samples_leaf=3 gives a better decision tree with smaller MDL
clf = tree.DecisionTreeClassifier(criterion='gini',min_samples_leaf=1, random_state=RandomState(130))
clf = clf.fit(training_data, training_class)

clf2 = tree.DecisionTreeClassifier(criterion='gini',min_samples_leaf=10, random_state=RandomState(130))
clf2 = clf2.fit(training_data, training_class)


print "training accyracy with overfitting=", clf.score(training_data,training_class)
print "training accyracy without overfitting=", clf2.score(training_data,training_class)

#print the decision tree in a pdf file
from sklearn.externals.six import StringIO
import pydot
dot_data = StringIO()
tree.export_graphviz(clf, out_file=dot_data)
graph = pydot.graph_from_dot_data(dot_data.getvalue())
graph.write_pdf("ecoli_OVERFIT.pdf")
dot_data = StringIO()
tree.export_graphviz(clf2, out_file=dot_data)
graph = pydot.graph_from_dot_data(dot_data.getvalue())
graph.write_pdf("ecoli_NO_OVERFIT.pdf")



#evaluate the decision tree on the testing set

print "test accyracy with overfitting=", clf.score(test_data,test_class)
print "test accyracy without overfitting=", clf2.score(test_data,test_class)

a=[]
for i in range(0,len(test_data)):
    if test_class[i]==clf.predict([test_data[i]]):
        a.append(1)
    else:
        a.append(0)
CI = stats.norm.interval(0.95,loc=sum(a)/float(len(a)),scale=stats.sem(a))
