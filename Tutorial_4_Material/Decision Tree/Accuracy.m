function [sum] = Accuracy(d,e)
%Calculate the average accuracy
sum=0;
for i=1:size(d,1)
if d(i)==e(i)
sum=sum+1;
end
end
sum=sum/size(d,1);
%fprintf('Pruned Decision Tree Average Accuracy: %f\n', sum);