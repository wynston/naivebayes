function [sr] = wordCounting(ss, k)
%
%INPUT:
%
%ss: name of the file e.g. apple 1.txt
%count: the k most frequent words
%
%
%OUTPUT:
%
%sr: the string of k most frequent words
%
%LI Xiaodong 04/14/2017

fidin=fopen(ss,'r');
s='';
while ~feof(fidin)
tline=fgetl(fidin);
s=[s tline];
end

str=s;
C = regexp((str),' ','split')';  
[val,idxC, idxV] = unique(C);
n = accumarray(idxV,1);
m = [n idxC] ;     %n occurences for word C(idxC)
y = [val num2cell(n)];       % t without sort
[~, so]= sort(n,'descend');     % sort the frequencies descend and rest alphabet
words= val(so);    % sort words by frequency 
freq= n(so);           % sort frequency
z = [words num2cell(freq)];    % show words with frequency sort
sr='';
for i=1:k
    sr=[sr ' ' strjoin(words(i))];
end