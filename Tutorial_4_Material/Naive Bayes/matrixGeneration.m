function y = matrixGenration( ss,docu )
%
%INPUT:
%
%ss: name of the feature words file e.g. better feature words for training.txt
%docu: name of the documents to generate a matrix column e.g. apple 0.txt
%
%OUTPUT:
%
%y: a column of document-term matrix (documents in columns)
%
%LI Xiaodong 04/14/2017


fidin=fopen(ss,'r');
s=fgetl(fidin);

fidin=fopen(docu,'r');
d=fgetl(fidin);

p=regexp(s, ' ', 'split');

y=[1:size(p,2)];

for i=1:size(p,2)
y(i)=size(strfind(d,p(i)),2);
end