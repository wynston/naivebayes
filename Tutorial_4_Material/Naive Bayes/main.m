function [] = main(k,n)
%
%INPUT:
%
%k: the number of files to be trained e.g. 5 apple.txt and 5 apple inc.txt
%n: total number of files to be trained e.g. 9 apple.txt and 9 apple inc.txt
%
%
%
%LI Xiaodong 04/14/2017

s1='apple ';
s2='apple inc ';
sname='keywords.txt';

y1=[];
for i=1:k
x1= matrixGeneration(sname,[s1 num2str(i-1) '.txt']);
y1=[y1;x1];
end

y2=[];
for i=1:k
x2= matrixGeneration(sname,[s2 num2str(i-1) '.txt']);
y2=[y2;x2];
end

y=[y1;y2];
y=y';

P=tfidf(y);
P=P';

ss1={'1'};
ss2={'2'};
b=[];
for i=1:k
b=[b;ss1];
end
for i=1:k
b=[b;ss2];
end

y=[];
for i=k+1:n
x1= matrixGeneration(sname,[s1 num2str(i-1) '.txt']);
x2= matrixGeneration(sname,[s2 num2str(i-1) '.txt']);
y=[y;x1;x2];
end
y=y';

Q=tfidf(y);
Q=Q';

d=[];
for i=k+1:n
d=[d;ss1;ss2];
end

A=[];
C=[];
for j=1:size(P,2)
%    sig=0;
%    for i=1:size(P,1)
%        if P(i,j)~=0
%            sig=1;
%        end
%    end
%    if sig==1
    if var(P(:,j))~=0 & var(P(1:k,j))~=0 & var(P(k+1:size(P,1),j))~=0
        A=[A P(:,j)];
        C=[C Q(:,j)];
    end
end

Naive_Bayes_Classifier(10,A,b,C,d);