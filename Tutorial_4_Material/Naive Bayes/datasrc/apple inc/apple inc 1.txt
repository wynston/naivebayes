Apple Computer, Inc. Finalizes Acquisition of NeXT Software Inc.

Company Moves Forward with Operating System Strategy Incorporating NeXT Technology

CUPERTINO, Calif.--Feb. 7, 1997--Apple Computer, Inc. today announced it has completed its acquisition of NeXT Software, Inc., after securing all necessary regulatory approvals. Apple announced on Dec. 20, 1996 its intention to purchase NeXT in a friendly acquisition for approximately $400 million. Steve Jobs, Chairman and CEO of NeXT Software, will serve as an advisor to Dr. Gilbert F. Amelio, Apple's Chairman and CEO.

"The finalization of our acquisition of NeXT confirms our commitment to providing our customers and software developers with a solid OS strategy," said Dr. Gilbert F. Amelio, Chairman and CEO, Apple Computer Inc. "We embark upon Apple's third decade with renewed enthusiasm, and we look forward to working with our developers, customers, and industry colleagues to proliferate Apple's legacy of vision and technological excellence."

The acquisition brings together Apple's and NeXT's innovative and complementary technology portfolios including Apple's leadership in ease-of-use and multimedia solutions and NeXT's strengths in development software and operating environments for both the enterprise and Internet markets. The combination significantly strengthens Apple's position as a company advancing industry standards.

NeXT's object oriented software development products will contribute to Apple's goal of creating a differentiated and profitable software business, with a wide range of products for enterprise, business, education, and home markets.

Earlier this year, Apple announced the company's operating system strategy, which incorporates NeXT technology. The Mac OS will continue to be upgraded in regular biannual releases, while NeXT technology will form the basis for Apple's next-generation OS, Rhapsody. Apple believes that the advanced technical underpinnings and rapid development environment of Rhapsody will allow developers to create new applications that leapfrog those of other 'modern' operating systems, such as Windows NT.

The first release of Rhapsody is expected to be launched to developers in mid to late 1997 and to customers within 12 months. A unified Rhapsody release is expected to be in the hands of customers by mid-1998. This will include compatibility with existing Mac OS applications, as well as provide a platform for next-generation computing.

The acquisition of NeXT is further evidence that Apple is fundamentally changing the way it does business. Apple believes that by embracing outside technology and driving cross-platform industry standards, it can innovate in the key areas that give its products and technology differentiation. NeXT's cross-platform development environments for enterprise and Internet/intranet markets, allow developers to write once and deploy across a range of Internet and client-server platforms.

In a new era of industry collaboration and joint initiatives--brought on by the "megatrends" of pervasive Internet and ubiquitous multimedia--NeXT technology complements Apple's strength in multimedia authoring and playback, as well as Internet access, Internet authoring, and Internet server solutions. In the last year Apple has worked on a series of collaboration initiatives which leverage the Company's core strengths in Internet, multimedia, and component software. Wide ranging agreements with Netscape Communications, Sun Microsystems, and Silicon Graphics Computer Systems--along with the acquisition of NeXT--confirm Apple is building strategic relationships at the forefront of the information industry.

Except for the historical information contained herein, the statements regarding effecting innovation, introducing new products, continuing focus on certain industry growth areas and are forward-looking statements that involve risks and uncertainties. Potential risks and uncertainties include, without limitation, continued competitive pressures in the marketplace; the effect competitive factors and the Company's reaction to them may have on consumer and business buying decisions with respect to the Company's products; the Company's ability to deliver successful innovative products to the marketplace; the Company's ability to deliver new products to the marketplace on a timely basis; the effect of the announced business restructuring on the future performance of the Company, especially the performance of the Company's employees; and the need for any future restructuring. More information on potential factors that could affect the Company's financial results are included in the Company's Form 10-Q for the first quarter of the 1997 fiscal year, to be filed with the SEC.

Apple Computer, Inc., a recognized innovator in the information industry and leader in multimedia technologies, creates powerful solutions based on easy-to-use personal computers, servers, peripherals, software, personal digital assistants and Internet content. Headquartered in Cupertino, California, Apple develops, manufactures, licenses and markets solutions, products, technologies and services for business, education, consumer, entertainment, scientific and engineering and government customers in more than 140 countries.

Press Information Contact:
Katie Cotton
Apple Computer, Inc.
(408) 974-7269
email: katiec@apple.com
Susan Lehman
Apple Computer, Inc.
(408) 974-0182
email: lehman@apple.com
NOTE TO EDITORS: To access Apple press releases, background material, and contact information on the web, visit The Source at: http://www.apple.com/source/. If you are interested in receiving Apple press releases by fax, call 1-800-AAPL-FAX (1-800-227-5329) and enter your PIN number. If you do not have a PIN number, contact Apple's Media Helpline at (408)974-2042 to request one. If you would like to receive Apple press releases by email, please send an email message to pressrel@thing2.info.apple.com. In the subject field, type the text "subscribe [your full name]."

