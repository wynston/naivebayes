World production of apples in 2014 was 84.6 million tonnes, with China producing 48% of this total (table).[3] Other major producers with 6% or less of the world total were the United States, Turkey, Poland and Italy.

Nutrition
Apples, with skin (edible parts)
Nutritional value per 100 g (3.5 oz)
Energy	218 kJ (52 kcal)
Carbohydrates
13.81 g
Sugars	10.39
Dietary fiber	2.4 g
Fat
0.17 g
Protein
0.26 g
Vitamins
Vitamin A equiv.
beta-carotene
lutein zeaxanthin
(0%) 3 μg
(0%) 27 μg
29 μg
Thiamine (B1)	(1%) 0.017 mg
Riboflavin (B2)	(2%) 0.026 mg
Niacin (B3)	(1%) 0.091 mg
Pantothenic acid (B5)	(1%) 0.061 mg
Vitamin B6	(3%) 0.041 mg
Folate (B9)	(1%) 3 μg
Vitamin C	(6%) 4.6 mg
Vitamin E	(1%) 0.18 mg
Vitamin K	(2%) 2.2 μg
Minerals
Calcium	(1%) 6 mg
Iron	(1%) 0.12 mg
Magnesium	(1%) 5 mg
Manganese	(2%) 0.035 mg
Phosphorus	(2%) 11 mg
Potassium	(2%) 107 mg
Sodium	(0%) 1 mg
Zinc	(0%) 0.04 mg
Other constituents
Water	85.56 g
Fluoride	3.3 µg
Link to Full Nutrient Report of USDA Database entry
Units
μg = micrograms • mg = milligrams
IU = International units
Percentages are roughly approximated using US recommendations for adults.
A typical apple serving weighs 242 grams and provides 126 calories with a moderate content of dietary fiber (table).[60] Otherwise, there is generally low content of essential nutrients (table).

Human consumption
See also: Cooking apple and Cider apple

An apple core, the remainder of an apple that has been mostly eaten
Apples are often eaten raw. The whole fruit including the skin is suitable for human consumption except for the seeds, which may affect some consumers.[citation needed] The core is often not eaten and is discarded. Cultivars bred for raw consumption are termed dessert or table apples.

Apples can be canned or juiced. They are milled or pressed to produce apple juice, which may be drunk unfiltered (called apple cider in North America), or filtered. The juice can be fermented to make cider (called hard cider in North America), ciderkin, and vinegar. Through distillation, various alcoholic beverages can be produced, such as applejack, Calvados,[61] and apfelwein. Apple seed oil[62] and pectin may also be produced.

Popular uses
Apples are an important ingredient in many desserts, such as apple pie, apple crumble, apple crisp and apple cake. They are often eaten baked or stewed, and they can also be dried and eaten or reconstituted (soaked in water, alcohol or some other liquid) for later use. When cooked, some apple cultivars easily form a puree known as apple sauce. Apples are also made into apple butter and apple jelly. They are also used (cooked) in meat dishes.

In the UK, a toffee apple is a traditional confection made by coating an apple in hot toffee and allowing it to cool. Similar treats in the U.S. are candy apples (coated in a hard shell of crystallized sugar syrup), and caramel apples, coated with cooled caramel.
Apples are eaten with honey at the Jewish New Year of Rosh Hashanah to symbolize a sweet new year.[61]
Farms with apple orchards may open them to the public, so consumers may themselves pick the apples they will purchase.[61]
Sliced apples turn brown with exposure to air due to the conversion of natural phenolic substances into melanin upon exposure to oxygen.[63] Different cultivars vary in their propensity to brown after slicing[64] and the genetically engineered Arctic Apples do not brown. Sliced fruit can be treated with acidulated water to prevent this effect.[63] Sliced apple consumption tripled in the US from 2004 to 2014 to 500 million apples annually due to its convenience.[65]

Organic production
Organic apples are commonly produced in the United States.[66] Due to infestations by key insects and diseases, organic production is difficult in Europe.[67] The use of pesticides containing chemicals, such as sulfur, copper, microorganisms, viruses, clay powders, or plant extracts (pyrethrum, neem) has been approved by the EU Organic Standing Committee to improve organic yield and quality.[67] A light coating of kaolin, which forms a physical barrier to some pests, also may help prevent apple sun scalding.[50]

Phytochemicals
Apples are a rich source of various phytochemicals including flavonoids (e.g., catechins, flavanols, and quercetin) and other phenolic compounds (e.g., epicatechin and procyanidins)[68] found in the skin, core, and pulp of the apple;[68] they have unknown health value in humans.[63]

Ideain (cyanidin 3-O-galactoside) is an anthocyanin, a type of pigment, which is found in some red apple cultivars.[69]

Phlorizin is a flavonoid that is found in apple trees, particularly in the leaves, and in only small amounts if at all in other plants, even other species of the Malus genus or related plants such as pear trees.[70]

Health effects
Preliminary research is investigating whether nutrients and/or phytochemicals in apples may affect the risk of some types of cancer.[68][71]

Allergy
One form of apple allergy, often found in northern Europe, is called birch-apple syndrome, and is found in people who are also allergic to birch pollen.[72] Allergic reactions are triggered by a protein in apples that is similar to birch pollen, and people affected by this protein can also develop allergies to other fruits, nuts, and vegetables. Reactions, which entail oral allergy syndrome (OAS), generally involve itching and inflammation of the mouth and throat,[72] but in rare cases can also include life-threatening anaphylaxis.[73] This reaction only occurs when raw fruit is consumed—the allergen is neutralized in the cooking process. The variety of apple, maturity and storage conditions can change the amount of allergen present in individual fruits. Long storage times can increase the amount of proteins that cause birch-apple syndrome.[72]


Different kinds of apple cultivars in a wholesale food market
In other areas, such as the Mediterranean, some individuals have adverse reactions to apples because of their similarity to peaches.[72] This form of apple allergy also includes OAS, but often has more severe symptoms, such as vomiting, abdominal pain and urticaria, and can be life-threatening. Individuals with this form of allergy can also develop reactions to other fruits and nuts. Cooking does not break down the protein causing this particular reaction, so affected individuals can eat neither raw nor cooked apples. Freshly harvested, over-ripe fruits tend to have the highest levels of the protein that causes this reaction.[72]

Breeding efforts have yet to produce a hypoallergenic fruit suitable for either of the two forms of apple allergy.[72]

Toxicity of seeds
The seeds of apples contain small amounts of amygdalin, a sugar and cyanide compound known as a cyanogenic glycoside. Ingesting small amounts of apple seeds will cause no ill effects, but consumption of extremely large doses can cause adverse reactions. It may take several hours before the poison takes effect, as cyanogenic glycosides must be hydrolyzed before the cyanide ion is released.[74] There are no case reports of amygdalin poisoning from consuming apple seeds as recorded in the Hazardous Substances Data Bank of the United States National Library of Medicine.[75]

Proverbs

An apple's side, stem end, and interior
The proverb "An apple a day keeps the doctor away", addressing the health effects of the fruit, dates from 19th century Wales, according to Caroline Taggart, author of “An Apple a Day: Old-Fashioned Proverbs and Why They Still Work”. The original phrase, Taggart said, was: "Eat an apple on going to bed, and you’ll keep the doctor from earning his bread". In the 19th century and early 20th, the phrase evolved to “an apple a day, no doctor to pay” and “an apple a days sends the doctor away”, while the phrasing now commonly used was first recorded in 1922.[76]