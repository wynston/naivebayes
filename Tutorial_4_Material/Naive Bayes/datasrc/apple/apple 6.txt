Some nutritionists say the trick to consuming more fruit is trying new fruit. (See you later, watermelon. Hello, guava!)
By Kelly DiNardo
Fruit
Photo: Mauricio Alejo

An apple a day may...drive you bananas. Eighty percent of Americans eat less than the recommended daily intake of fruit (one and a half to two cups for women), and some experts blame this shortfall on "fruit fatigue." It turns out that four staples account for nearly half of the fruit consumption among women: apples, bananas, oranges, and watermelon. But when you eat the same foods over and over again, says Joan Salge Blake, a spokesperson for the Academy of Nutrition and Dietetics, "you get bored and wind up not wanting to eat them at all." 

As a result, you could be missing out on a host of benefits, from cancer-fighting agents to improved cardiovascular health. A recent French study discovered that a diet rich in fruit might even help preserve your memory as you age. "Plus, the more varied your diet," Blake notes, "the more likely you are to get all the vitamins, essential minerals, and phytonutrients you need." Next, a guide to expanding your fruit palate.