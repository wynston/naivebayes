function [] = Naive_Bayes_Classifier(k,A,b,C,d)
%
% This function will:
% 
% 1. train a naive bayes classifier based on 'training_data.txt' and 'training_class.txt';
% 2. perfom resubstitution validation and k-fold cross validation;
% 3. predict lables for 'test_data.txt';
% 4. output the average accuracy of this classifier based on 'test_class.txt'.
%
%
% INPUT:         
% k: K-fold cross validation partition
% A: training matrix
% b: training labels
% C: test matrix
% d: test label
%
% author: LI Xiaodong 04/12/2017


% input data A and labels b

%[a1 a2 a3 a4]=textread('training_data.txt','%s%s%s%s');
%a2 is not helpful as they are all same with each other
%It asks variance not equal 0
%A=[str2double(a1) str2double(a3) str2double(a4)];
%b=textread('training_class.txt','%s');



%K-fold cross validation partition
cp = cvpartition(b,'KFold',k)

%train the Naive Bayes Classifiers by A, b
nbGau = fitcnb(A, b);

%output the resubstitution error 
Resubstitution_Error = resubLoss(nbGau)

%output the cross-validation error 
%train the classifier with training set A and labels b
nbGauCV = crossval(nbGau, 'CVPartition',cp);
Cross_Validation_Error  = kfoldLoss(nbGauCV)

%input the test data C and test lable d

%[a1 a2 a3 a4]=textread('test_data.txt','%s%s%s%s');
%C=[str2double(a1) str2double(a3) str2double(a4)];
%d=textread('test_class.txt','%s');

%predict by classifier nbGau and test set C
Predict_Labels = predict(nbGau, C);

%Calculate the average accuracy
d=str2double(d);
e=str2double(Predict_Labels);
sum=0;
for i=1:size(d,1)
if d(i)==e(i)
sum=sum+1;
if d(i)==1
fprintf('Successfully predict ducument %d as apple the fruit\n', i);
end
if d(i)==2
fprintf('Successfully predict ducument %d as apple the company\n', i);
end
end
end
sum=sum/size(d,1);
fprintf('Average Accuracy: %f\n', sum);