# -*- coding: utf-8 -*-
# data files -> /datasrc/
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.cross_validation import train_test_split
import numpy as np
import codecs

dataFiles = load_files('./datasrc/')

# Question 3:
train_raw, test_raw, class_train, class_test = train_test_split(dataFiles.data, dataFiles.target, test_size = 0.3)
count_vect = CountVectorizer(stop_words='english')

train, test = list(), list()
for content in train_raw:
    str=unicode(content, errors='ignore')
    #print('train', str)
    #print(content)
    train.append(str)
for content in test_raw:
    str=unicode(content, errors='ignore')
    #print('test', str)
    test.append(str)

X_train = count_vect.fit_transform(train)
X_test = count_vect.transform(test)

# training part
clf = MultinomialNB()
clf.fit(X_train, class_train)
class_predicted = clf.predict(X_test)

print(('Question 3: Classifier Accuracy: {}%').format(np.mean(class_predicted == class_test) * 100))

# Question 4:
